import {BrowserRouter,Routes, Route,Navigate} from "react-router-dom";
import {Registration} from "../Components/Registration";
import {Home} from "../Components/Home";

export const Navigations = () => {
    
    return (
        <BrowserRouter>
            <Routes>
                <Route path="register/:code" element={<Registration/>} />
                <Route path="home" element={<Home/>} />
                <Route path="*" element={<Navigate to={'register/null'}/>}/>
            </Routes>
        </BrowserRouter>
    )
}