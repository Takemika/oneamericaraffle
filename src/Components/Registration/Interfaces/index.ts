import {Locale} from "antd/es/locale-provider";
import {RadioChangeEvent} from "antd";

export interface Radio {
	label: string;
	value: boolean;
}

export interface Formulario {
	email:string;
	emailPlaceholder:string;
	phone:string;
	phonePlaceholder:string;
	name:string;
	namePlaceholder:string;
	question:string;
	button:string;
	legend:string;
}

export interface RegistrationFormPros {
	formulario?:Formulario;
	crypto?:Radio[];
	referralCode:string;
	RegisterMail: () => void;
	activerCompartir:boolean;
	Comaprtir: () => void;
	activerRegistrar:boolean;
}

export interface IdiomsPros {
	locale:Locale;
	changeLocale: (e: RadioChangeEvent) => void;
}

export interface UserFormulario {
	email:string;
	phone:string;
	name:string;
	question:boolean;
}

export interface ModaInfo {
	share:string;
	titleModal:string;
	title:string;
	paragraph:string
}