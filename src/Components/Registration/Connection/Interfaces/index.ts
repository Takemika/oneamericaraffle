export interface User {
	tradeCrypto:boolean;
	phone:string;
	name:string;
	email:string;
	createdAt:Date;
	code:string;
}

export interface Referral {
	createdAt:Date;
	idUserReferrals:string;
	idUsers:string
}

export interface Email {
	correo:string;
	compartir:boolean;
}