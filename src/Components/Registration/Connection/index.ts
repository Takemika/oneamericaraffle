import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { collection, addDoc,getDoc,doc,getDocs,runTransaction } from "firebase/firestore";
import {User, Referral, Email} from "./Interfaces";
import {APIKEY, APPID, MESSAGINGSENDERID, PROJECTID} from "../../../Settings/Firebase";

const firebaseConfig = {
	apiKey: APIKEY,
	authDomain: PROJECTID+".firebaseapp.com",
	projectId: PROJECTID,
	storageBucket: PROJECTID+".appspot.com",
	messagingSenderId: MESSAGINGSENDERID,
	appId: APPID
};

const app = initializeApp(firebaseConfig);
const DB = getFirestore(app);

export const getUser = async (code?:string) => {
	let codigo = code?? '';
	const documento = doc(DB,'emails',codigo)
	const data = await getDoc(documento)
	if (data.exists()){
		return data.id
	}else{
		return undefined;
	}
}
export const AddUSer = async (user:User) => {
	const coleccion = collection(DB,'users');
	const Usuario = await addDoc(coleccion,user);
	return Usuario.id;
}
export const AddEmail = async (email:Email) => {
	const coleccion = collection(DB,'emails');
	const Correo = await addDoc(coleccion,email);
	return Correo.id;
}
export const Referrals = async (user:Referral) => {
	const coleccion = collection(DB,'referrals');
	await addDoc(coleccion,user);
}
export const SearchMail = async (email:string) => {
	const coleccion = collection(DB,'emails');
	const emails = await getDocs(coleccion);
	let encontrado:boolean = false;
	const info = emails.docs.map(doc => doc.data())
	info.forEach((e)=>{
		if (e.correo === email) encontrado = true;
	});
	return encontrado;
}
export const ActivateMail = async (code:string) => {
	let codigo = code?? '';
	const coleccion = doc(DB,'emails',codigo);
	try {
		const newPopulation = await runTransaction(DB,async (transaction)=>{
			const sfDoc = await transaction.get(coleccion);
			if (!sfDoc.exists()){
				throw "Document does not exist!";
			}
			transaction.update(coleccion,{compartir:true});
		});
		console.log("Population increased to ", newPopulation);
	}catch (e) {
		console.log(e);
	}
}