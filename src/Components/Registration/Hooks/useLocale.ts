import {useEffect, useState} from "react";
import {RadioChangeEvent} from "antd";
import moment from "moment";
import esEs from "antd/lib/locale/es_ES";
import {Formulario, ModaInfo, Radio} from "../Interfaces";
import {FormualrioEs, ModaEs, RadioEs} from "../Locales/esEs";
import {FormualrioEn, ModaEn, RadioEn} from "../Locales/enUs";

export const useLocale = () => {
	
	const [ locale, setLocale ] = useState(esEs);
	const [ crypto,setCrypto ] = useState<Radio[]>();
	const [ formulario, setFormulario] = useState<Formulario>();
	const [modal,setModal] = useState<ModaInfo>()
	
	useEffect(()=>{
		CryptoSet()
	},[]);
	useEffect(()=>{
		CryptoSet()
	},[locale]);
	
	const changeLocale = (e: RadioChangeEvent) => {
		const localeValue = e.target.value;
		setLocale(localeValue)
		
		if (!localeValue) {
			moment.locale('es');
		}else {
			moment.locale('en');
		}
	}
	const CryptoSet = () =>{
		let data:Radio[];
		let formu:Formulario;
		let Modal:ModaInfo;
		if (locale === esEs) {
			formu = FormualrioEs;
			data = RadioEs;
			Modal = ModaEs;
		}else {
			formu = FormualrioEn;
			data = RadioEn;
			Modal = ModaEn;
		}
		setModal(Modal);
		setFormulario(formu);
		setCrypto(data);
	}
	return {
		locale,crypto,formulario,modal,
		changeLocale,CryptoSet
	}
}