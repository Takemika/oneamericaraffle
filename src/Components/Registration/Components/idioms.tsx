import esEs from "antd/lib/locale/es_ES";
import enUS from "antd/lib/locale/en_US";
import {Image, Radio} from "antd";
import { IdiomsPros } from "../Interfaces";
import logo from "../../../fondo.jpg"


export const Idioms = ({locale,changeLocale}:IdiomsPros) =>{
	return (
		<>
			<div style={{paddingTop: '20px',display:"flex",
				justifyContent: "flex-end",position: "fixed",
				right:"0",zIndex:"2"}}>
				<Radio.Group value={locale} onChange={changeLocale} style={{ padding: '0 50px' }}>
					<Radio.Button key="en" value={enUS}>
						EN
					</Radio.Button>
					<Radio.Button key="es" value={esEs}>
						ES
					</Radio.Button>
				</Radio.Group>
			</div>
			<img src={logo} style={{height:"140px",width:"100%",objectFit:"cover",objectPosition:'center'}}/>
		</>
		
	)
}