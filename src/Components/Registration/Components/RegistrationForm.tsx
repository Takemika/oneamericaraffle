import { Button, Col, Form, Input, Radio,Space } from "antd";
import { Typography } from "antd";
import { WhatsAppOutlined } from "@ant-design/icons";
import { RegistrationFormPros } from "../Interfaces";
import { URLWH, invitationtext_en } from "../../../Settings/env";
const { Title ,Text} = Typography;

export const RegistrationForm = ({
  formulario,
  crypto,
  referralCode,
  RegisterMail,
  activerCompartir,
  Comaprtir,
  activerRegistrar,
}: RegistrationFormPros) => {
  const Compartir = () => {
    Comaprtir();
    const tagA = document.createElement("a");
    tagA.href = URLWH + invitationtext_en+" "+referralCode;
    tagA.target = "_blank";
    tagA.click();
  };
  return (
    <Col>
      <Title>500 US Dollars for 5 million winners</Title>
	  <Space direction="vertical">
      <Text >
        The One Americas is conducting the largest Giveaway in history, giving 100,000 people a chance to win 500
        Dollars each, every week for 50 weeks
      </Text>
	  <Text >
	  Register below to qualify
      </Text>

      <Form.Item
        label={formulario?.email}
        name={"email"}
        rules={[{ required: true, type: "email" }]}
        style={{ marginBottom: "10px" }}
      >
        <Input placeholder={formulario?.emailPlaceholder} onBlur={() => RegisterMail()} />
      </Form.Item>
      <Form.Item name="phone" label={formulario?.phone} style={{ marginBottom: "10px" }}>
        <Input placeholder={formulario?.phonePlaceholder} style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item label={formulario?.name} name={"name"} style={{ marginBottom: "10px" }}>
        <Input placeholder={formulario?.namePlaceholder} />
      </Form.Item>
      <label style={{ color: "darkgray" }}>{formulario?.legend}</label>
      <Form.Item style={{ marginBottom: "10px" }} label={formulario?.question} name={"question"}>
        <Radio.Group options={crypto} />
      </Form.Item>
      <Form.Item>
        <Button disabled={activerCompartir} onClick={Compartir} shape="circle" size={"large"}>
          {activerCompartir ? <WhatsAppOutlined /> : <WhatsAppOutlined style={{ fontSize: "30px", color: "green" }} />}
        </Button>
      </Form.Item>
      <Form.Item>
        <Button disabled={activerRegistrar} type="primary" htmlType="submit" block size={"large"}>
          {formulario?.button}
        </Button>
      </Form.Item>
	  </Space>
    </Col>
  );
};
