export const FormualrioEs = ({
	email: 'Correo', emailPlaceholder: 'Por favor escribe su Correo',
	phone: 'Teléfono',phonePlaceholder: 'Por favor escribe su Teléfono',
	name: 'Nombre',namePlaceholder:'Por favor escribe su Nombre',
	question: '¿Negocia o mantiene cripto?',button:'Registrar',
	legend:'Comparte esta invitación con al menos 3 contactos para poder participar'
})
export const RadioEs = ([
	{label: 'Si', value: true},
	{label: 'no', value: false}
]);
export const ModaEs = ({
	share: 'Comparte el enlace para acercar a tus referidos.',titleModal: 'Página de agradecimiento.',
	title: 'Gracias',paragraph: 'Mantenga la descripción breve e informativa.'
})