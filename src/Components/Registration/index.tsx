import {Form, ConfigProvider, Row,Modal,notification } from 'antd';
import {useLocale} from "./Hooks/useLocale";
import {Idioms} from "./Components/idioms";
import { useParams } from 'react-router-dom'
import { RegistrationForm } from './Components/RegistrationForm';
import {Email, Referral, User} from './Connection/Interfaces';
import {ActivateMail, AddEmail, AddUSer, getUser, Referrals, SearchMail} from "./Connection";
import {useEffect, useState} from "react";
import { UserFormulario } from './Interfaces';
import {utf8_to_b64, validateMessages } from './Utilities';
import {URL} from "../../Settings/env";

export const Registration = () => {
	const code = useParams<'code'>();
	const { locale,crypto,formulario,modal,changeLocale } = useLocale();
	const [form] = Form.useForm<UserFormulario>();
	const [referralCode,setReferralCode] = useState<string>('');
	const [userReferrals,setUserReferrals] = useState<string>();
	const [newCode,setNewCode] = useState<string>('');
	const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
	const [activerCompartir, setActiverCompartir] = useState<boolean>(true);
	const [activerRegistrar, setActiverRegistrar] = useState<boolean>(true);

	useEffect(()=>{
		const codigo = code.code;
		const optenerUser = async () => {
			let cod = await getUser(codigo);
			setUserReferrals(cod);
		}
		optenerUser().then();
	},[]);
	const onClose = () => {
		setIsModalVisible(false);
	};
	const RegisterMail = async () => {
		const values = await form.validateFields();
		const encontrado = await SearchMail(values.email);
		if (encontrado) {
			openNotification();
		}else{
			const data:Email = { correo:values.email,compartir:false }
			let referralCode = await AddEmail(data);
			referralCode = URL+referralCode;
			setNewCode(referralCode);
			setActiverCompartir(false);
			setReferralCode(referralCode);
		}
	};
	const Comaprtir = () => {
		setActiverRegistrar(false);
	}
	const openNotification = () => {
		notification.open({
			message: 'Error',
			description: 'lo sentimos este correo ya está registrado'
		});
	}
	const onFinis = async () => {
		const values = await form.validateFields();
		let user:User;
		let date:Date = new Date()
		user = {
			tradeCrypto: values.question, phone: values.phone,
			name: values.name, email: values.name,createdAt: date,
			code: newCode
		}
		const id = await AddUSer(user);
		if (userReferrals){
			let dato:Referral = {
				createdAt: date,idUsers:id,idUserReferrals:userReferrals
			}
			await Referrals(dato);
		}
		setIsModalVisible(true)
	}
	return (
		<>
			<Modal
				title={modal?.titleModal}
				visible={isModalVisible}
				onCancel={onClose}
			>
				<h2 style={{textAlign:'center'}}>{modal?.title}</h2>
				<p style={{textAlign:'center'}}>
					{modal?.paragraph}
				</p>
			</Modal>
			<Idioms locale={locale} changeLocale={changeLocale}/>
			<div style={{ padding: '20px 50px' }}>
				<ConfigProvider locale={locale}>
					<Form
						form={form}
						layout="vertical"
						validateMessages={validateMessages}
						size={'small'}
						onFinish={onFinis}
					>
						<Row align={'middle'} justify={'center'}>
							<RegistrationForm activerRegistrar={activerRegistrar} formulario={formulario} crypto={crypto} referralCode={referralCode} RegisterMail={RegisterMail} activerCompartir={activerCompartir} Comaprtir={Comaprtir} />
						</Row>
					</Form>
				</ConfigProvider>
			</div>
		</>
	);
};