import React, {FC} from 'react';
import './App.css';
import {Layout} from 'antd';
import {Navigations} from "./routes/Navigations";

const { Content } = Layout;

const App:FC = () => {
	return (
		<Layout className='app'>
			<Content style={{height:'100vh'}}>
				<Navigations/>
			</Content>
		</Layout>
	);
}

export default App;
